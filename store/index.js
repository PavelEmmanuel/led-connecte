export const state = () => ({
    message: '',
    authenticated: false
})

export const mutations = {
    set_message(state, message) {
        state.message = message
    },

    set_authenticated(state, result) {
        state.authenticated = result
    },

    initialize(state) {
        console.log("initializing state")
        state.authenticated = false
        if (localStorage.getItem('hasiottoken')) {
            state.authenticated = true
        }
    }
}

export const getters = {
    authenticated: state => state.authenticated,
    message: state => state.message
}