import Vue from 'vue'
import Tawk from 'vue-tawk'
  
Vue.use(Tawk, {
    tawkSrc: 'https://embed.tawk.to/5de858c8d96992700fcace47/default'
})